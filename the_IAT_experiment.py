from __future__ import absolute_import, division
from psychopy import locale_setup, sound, gui, visual, core, data, event, logging, clock
import numpy as np  # whole numpy lib is available, prepend 'np.'
from numpy.random import random, randint, normal, shuffle
import os  # handy system and path functions
import sys  # to get file system encoding
import pandas as pd
import glob, os, shutil
import random


# Create popup information box
popup = gui.Dlg(title = "IAT experiment")
popup.addField("Participant ID: ") # Empty box
popup.addField("Age: ")
popup.addField("Gender: ", choices=["Male", "Female"]) # Dropdown menu
popup.addField("Education: ", choices=["Cognitive Science", "Philosophy", "Biology"])
popup.addField("Current semester: ", choices=["2", "4", "6", "8", "10"])
popup.show()
if popup.OK: # To retrieve data from popup window
    ID = popup.data
elif popup.Cancel: # To cancel the experiment if popup is closed
    core.quit()



#introduction text 
intro = '''Welcome to the Implicit Assosication Task!

You will be using the 'E' and 'I' computer keys to categorize items into groups as fast as you can. These are the four groups and the items that belong to each:


Male:
Man, Son, Father, Boy, Uncle, Grandpa, Husband, Male

Female:
Mother, Wife, Aunt, Woman, Girl, Female, Grandma, Daughter

Science:
Astronomy, Math, Chemistry, Physics, Biology, Geology, Engineering, Neuroscience 

Liberal Arts:
History, Arts, Humanities, English, Philosophy, Music, Literature, Latin


There are seven parts. The instructions change for each part. Pay attention!

Press "space" to continue'''


# define window
win = visual.Window(fullscr=True, color = 'White')

# define a stop watch
stopwatch = core.Clock()

# prepare stimuli
WordArt = ['History', 'Arts', 'Humanities', 'English', 'Philosophy', 'Music', 'Latin', 'Literature',
'History', 'Arts', 'Humanities', 'English', 'Philosophy', 'Music', 'Latin', 'Literature']

WordSci = ['Astronomy', 'Math','Chemistry', 'Physics', 'Biology', 'Neuroscience', 'Engineering', 'Geology', 
'Astronomy', 'Math', 'Chemistry', 'Physics', 'Biology', 'Neuroscience', 'Engineering', 'Geology']

WordScience = WordArt + WordSci



WordMale = ['Man', 'Son', 'Father', 'Boy', 'Uncle', 'Grandpa', 'Husband', 'Male', 
'Man', 'Son', 'Father', 'Boy', 'Uncle', 'Grandpa', 'Husband', 'Male']

WordFemale = ['Mother', 'Wife', 'Aunt', 'Woman', 'Girl', 'Female', 'Grandma', 'Daughter', 
'Mother', 'Wife', 'Aunt', 'Woman', 'Girl', 'Female', 'Grandma', 'Daughter']

WordGender = WordMale + WordFemale

WordAll = WordScience + WordGender


WordScience20 = random.sample(WordScience, 20)
WordGender20 = random.sample(WordGender, 20)
WordScience28 = random.sample(WordScience, 28)
WordAll20 = random.sample(WordAll, 20)
WordAll40 = random.sample(WordAll, 40)


trial = (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 
21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40)


# prepare pandas data frame for recorded data
columns = ['ID', 'age', 'gender', 'word', 'response', 'correct', 'rt'] 
index = np.arange(0)
iat_data = pd.DataFrame(columns=columns, index = index)

# define function that shows text
def msg(txt):
    instructions = visual.TextStim(win, text=txt, color = 'Black', height = 0.05) # create an instruction text
    instructions.draw() # draw the text stimulus in a "hidden screen" so that it is ready to be presented 
    win.flip() # flip the screen to reveal the stimulusi
    event.waitKeys(keyList = ['space'])


# show instructions
msg(intro) #this text is above 


block1 = '''Now put your middle or index fingers on the E and I keys of your keyboard. Words representing the categories at the top will appear one-by-one in the middle of the screen. 

When the item belongs to a category on the left, press the E key; when the item belongs to a category on the right, press the I key. Items belong to only one category. If you make an error, an X will appear - fix the error by hitting the other key.

This is a timed sorting task. GO AS FAST AS YOU CAN while making as few mistakes as possible.

Press "space" when you are ready to start the experiment'''


# define function that shows text
def msg1(txt):
    instructions = visual.TextStim(win, text=txt, color = 'Black', height = 0.05) # create an instruction text
    stimulusLeft = visual.TextStim(win, text= 'Arts', pos = (-0.7, 0.8), color = "green")
    stimulusRight = visual.TextStim(win, text= 'Science', pos = (0.7, 0.8), color = "green")
    instructions.draw()
    stimulusLeft.draw()
    stimulusRight.draw() # draw the text stimulus in a "hidden screen" so that it is ready to be presented 
    win.flip() # flip the screen to reveal the stimulusi
    event.waitKeys(keyList = ['space'])

msg1(block1)

previous = None #just leave this the same variable for all blocks 

print(previous)

###BLOCK 1
# loop through trials
for i in range(len(WordScience20)):
    # prepare stimulus
    while WordScience20[i] == previous:
        WordScience20[i] = random.sample(WordScience20,1)[0]
        if WordScience20[i] != previous: 
            break
    stimulus = visual.TextStim(win, text=WordScience20[i], color = "green")
    stimulusLeft = visual.TextStim(win, text= 'Arts', pos = (-0.7, 0.8), color = "green")
    stimulusRight = visual.TextStim(win, text= 'Science', pos = (0.7, 0.8), color = "green")
    # draw stimulus
    stimulus.draw()
    stimulusLeft.draw()
    stimulusRight.draw()
    win.flip()
    # reset stop watch
    stopwatch.reset()                              #reset the clock to 0:0:0 
    # record key press
    key = event.waitKeys(keyList = ['escape', 'e', 'i'])                  # wait for any key press
    # get reaction time at key press
    reaction_time = stopwatch.getTime()            # asks the stopwatch for the time since reset and save to the variable reation_time
    
    # check if response is correct and if it is congruent 
    if key[0] == 'e' and WordScience20[i] in WordArt:
        correct = 1
    elif key[0] == 'i' and WordScience20[i] in WordSci:
        correct = 1
    elif key[0] == 'escape':
        core.quit()
        win.close()
    elif key[0] == 'e' and WordScience20[i] in WordSci:
        stimulus = visual.TextStim(win, text=WordScience20[i], color = "green")
        stimulus.draw()
        stimulusLeft = visual.TextStim(win, text= 'Arts', pos = (-0.7, 0.8), color = "green")
        stimulusRight = visual.TextStim(win, text= 'Science', pos = (0.7, 0.8), color = "green")
        stimulusLeft.draw()
        stimulusRight.draw()
        stimulusX = visual.TextStim(win, text='X', pos = (0.0, -0.6), color = "red")
        stimulusX.draw()
        win.flip()
        correct = 0
        keyX = event.waitKeys(keyList = ['escape', 'i'])
    else: 
        stimulus = visual.TextStim(win, text=WordScience20[i], color = "green")
        stimulus.draw()
        stimulusLeft = visual.TextStim(win, text= 'Arts', pos = (-0.7, 0.8), color = "green")
        stimulusRight = visual.TextStim(win, text= 'Science', pos = (0.7, 0.8), color = "green")
        stimulusLeft.draw()
        stimulusRight.draw()
        stimulusX = visual.TextStim(win, text='X', pos = (0.0, -0.6), color = "red")
        stimulusX.draw()
        win.flip()
        correct = 0
        keyX = event.waitKeys(keyList = ['escape', 'e'])
  # append all recorded data to the pandas DATA 
    iat_data = iat_data.append({
        'ID': ID[0],                #make these match the info box 
        'age': ID[1],
        'gender': ID[2],
        'education': ID[3],
        'semester': ID[4],
        'word': WordScience20[i],
        'response': key[0],
        'correct': correct, 
        'rt': reaction_time,
        'block': '1', #change for very loop/block
        'trial': trial[i]
        }, ignore_index=True)
    previous = WordScience20[i]



block2 = '''See above, the categories have changed. The items for sorting have changed as well. The rules, however, are the same.

When the item belongs to a category on the left, press the E key; when the item belongs to a category on the right, press the I key. Items belong to only one category. An X appears after an error - fix the error by hitting the other key. GO AS FAST AS YOU CAN.

Press "space" to continue'''


# define function that shows text
def msg2(txt):
    instructions = visual.TextStim(win, text=txt, color = 'Black', height = 0.05) # create an instruction text
    stimulusLeft = visual.TextStim(win, text= 'Male', pos = (-0.7, 0.8), color = "blue")
    stimulusRight = visual.TextStim(win, text= 'Female', pos = (0.7, 0.8), color = "blue")
    instructions.draw()
    stimulusLeft.draw()
    stimulusRight.draw() # draw the text stimulus in a "hidden screen" so that it is ready to be presented 
    win.flip() # flip the screen to reveal the stimulusi
    event.waitKeys(keyList = ['space'])


msg2(block2)


###BLOCK 2 
# loop through trials
for i in range(len(WordGender20)):
    # prepare stimulus
    while WordGender20[i] == previous: 
        WordGender20[i] = random.sample(WordGender20,1)[0]
        if WordGender20[i] != previous: 
            break
    stimulus = visual.TextStim(win, text=WordGender20[i], color = "blue")
    stimulusLeft = visual.TextStim(win, text= 'Male', pos = (-0.7, 0.8), color = "blue")
    stimulusRight = visual.TextStim(win, text= 'Female', pos = (0.7, 0.8), color = "blue")
    # draw stimulus
    stimulus.draw()
    stimulusLeft.draw()
    stimulusRight.draw()
    win.flip()
    # reset stop watch
    stopwatch.reset()                              #reset the clock to 0:0:0 
    # record key press
    key = event.waitKeys(keyList = ['escape', 'e', 'i'])                  # wait for any key press
    # get reaction time at key press
    reaction_time = stopwatch.getTime()            # asks the stopwatch for the time since reset and save to the variable reation_time
    
    # check if response is correct and if it is congruent 
    if key[0] == 'e' and WordGender20[i] in WordMale:
        correct = 1
    elif key[0] == 'i' and WordGender20[i] in WordFemale:
        correct = 1
    elif key[0] == 'escape':
        core.quit()
        win.close()
    elif key[0] == 'e' and WordGender20[i] in WordFemale:
        stimulus = visual.TextStim(win, text=WordGender20[i], color = "blue")
        stimulus.draw()
        stimulusLeft = visual.TextStim(win, text= 'Male', pos = (-0.7, 0.8), color = "blue")
        stimulusRight = visual.TextStim(win, text= 'Female', pos = (0.7, 0.8), color = "blue")
        stimulusLeft.draw()
        stimulusRight.draw()
        stimulusX = visual.TextStim(win, text='X', pos = (0.0, -0.6), color = "red")
        stimulusX.draw()
        win.flip()
        correct = 0
        keyX = event.waitKeys(keyList = ['escape', 'i'])
    else: 
        stimulus = visual.TextStim(win, text=WordGender20[i], color = "blue")
        stimulus.draw()
        stimulusLeft = visual.TextStim(win, text= 'Male', pos = (-0.7, 0.8), color = "blue")
        stimulusRight = visual.TextStim(win, text= 'Female', pos = (0.7, 0.8), color = "blue")
        stimulusLeft.draw()
        stimulusRight.draw()
        stimulusX = visual.TextStim(win, text='X', pos = (0.0, -0.6), color = "red")
        stimulusX.draw()
        win.flip()
        correct = 0
        keyX = event.waitKeys(keyList = ['escape', 'e'])
  # append all recorded data to the pandas DATA 
    iat_data = iat_data.append({
        'ID': ID[0],                #make these match the info box 
        'age': ID[1],
        'gender': ID[2],
        'education': ID[3],
        'semester': ID[4],
        'word': WordGender20[i],
        'response': key[0],
        'correct': correct, 
        'rt': reaction_time,
        'block': '2', #change for very loop/block
        'trial': trial[i]
        }, ignore_index=True)
    previous = WordGender20[i]



block3 = '''See above, the four categories you saw separately now appear together. Remember, each item belongs to only one group.

The green and blue labels and items may help to identify the appropriate category. Use the E and I keys to categorize items into four groups left and right, and correct errors by hitting the other key.

Press "space" to continue'''


# define function that shows text
def msg3(txt):
    instructions = visual.TextStim(win, text=txt, color = 'Black', height = 0.05) # create an instruction text
    stimulusLeft = visual.TextStim(win, text= 'Male', pos = (-0.7, 0.8), color = "blue")
    stimulusLeftOr = visual.TextStim(win, text= 'or', pos = (-0.7, 0.7), color = "Black")
    stimulusLeft2 = visual.TextStim(win, text= 'Arts', pos = (-0.7, 0.6), color = "green")
    stimulusRight = visual.TextStim(win, text= 'Female', pos = (0.7, 0.8), color = "blue")
    stimulusRightOr = visual.TextStim(win, text= 'or', pos = (0.7, 0.7), color = "Black")
    stimulusRight2 = visual.TextStim(win, text= 'Science', pos = (0.7, 0.6), color = "green")
    instructions.draw()
    stimulusLeft.draw()
    stimulusLeftOr.draw()
    stimulusLeft2.draw()
    stimulusRight.draw()
    stimulusRightOr.draw() 
    stimulusRight2.draw() # draw the text stimulus in a "hidden screen" so that it is ready to be presented 
    win.flip() # flip the screen to reveal the stimulusi
    event.waitKeys(keyList = ['space'])


msg3(block3)


###BLOCK 3
# loop through trials
for i in range(len(WordAll20)):
    # prepare stimulus
    while WordAll20[i] == previous: 
        WordAll20[i] = random.sample(WordAll20,1)[0]
        if WordAll20[i] != previous: 
            break
    if WordAll20[i] in WordScience:
        stimulus = visual.TextStim(win, text=WordAll20[i], color = "green")
    else:
        stimulus = visual.TextStim(win, text=WordAll20[i], color = "blue")
    stimulusLeft = visual.TextStim(win, text= 'Male', pos = (-0.7, 0.8), color = "blue")
    stimulusLeftOr = visual.TextStim(win, text= 'or', pos = (-0.7, 0.7), color = "Black")
    stimulusLeft2 = visual.TextStim(win, text= 'Arts', pos = (-0.7, 0.6), color = "green")
    stimulusRight = visual.TextStim(win, text= 'Female', pos = (0.7, 0.8), color = "blue")
    stimulusRightOr = visual.TextStim(win, text= 'or', pos = (0.7, 0.7), color = "Black")
    stimulusRight2 = visual.TextStim(win, text= 'Science', pos = (0.7, 0.6), color = "green")
    # draw stimulus
    stimulus.draw()
    stimulusLeft.draw()
    stimulusLeftOr.draw()
    stimulusLeft2.draw()
    stimulusRight.draw()
    stimulusRightOr.draw()
    stimulusRight2.draw()
    win.flip()
    # reset stop watch
    stopwatch.reset()                              #reset the clock to 0:0:0 
    # record key press
    key = event.waitKeys(keyList = ['escape', 'e', 'i'])                  # wait for any key press
    # get reaction time at key press
    reaction_time = stopwatch.getTime()            # asks the stopwatch for the time since reset and save to the variable reation_time
    
    # check if response is correct and if it is congruent 
    if key[0] == 'e' and (WordAll20[i] in WordArt or WordAll20[i] in WordMale):
        correct = 1
    elif key[0] == 'i' and (WordAll20[i] in WordSci or WordAll20[i] in WordFemale):
        correct = 1
    elif key[0] == 'escape':
        core.quit()
        win.close()
    elif key[0] == 'e' and (WordAll20[i] in WordSci or WordAll20[i] in WordFemale):
        if WordAll20[i] in WordScience:
            stimulus = visual.TextStim(win, text=WordAll20[i], color = "green")
        else:
            stimulus = visual.TextStim(win, text=WordAll20[i], color = "blue")
        stimulusLeft = visual.TextStim(win, text= 'Male', pos = (-0.7, 0.8), color = "blue")
        stimulusLeftOr = visual.TextStim(win, text= 'or', pos = (-0.7, 0.7), color = "Black")
        stimulusLeft2 = visual.TextStim(win, text= 'Arts', pos = (-0.7, 0.6), color = "green")
        stimulusRight = visual.TextStim(win, text= 'Female', pos = (0.7, 0.8), color = "blue")
        stimulusRightOr = visual.TextStim(win, text= 'or', pos = (0.7, 0.7), color = "Black")
        stimulusRight2 = visual.TextStim(win, text= 'Science', pos = (0.7, 0.6), color = "green")
        stimulus.draw()
        stimulusLeft.draw()
        stimulusLeftOr.draw()
        stimulusLeft2.draw()
        stimulusRight.draw()
        stimulusRightOr.draw()
        stimulusRight2.draw()
        stimulusX = visual.TextStim(win, text='X', pos = (0.0, -0.6), color = "red")
        stimulusX.draw()
        win.flip()
        correct = 0
        keyX = event.waitKeys(keyList = ['escape', 'i'])
    else: 
        if WordAll20[i] in WordScience:
            stimulus = visual.TextStim(win, text=WordAll20[i], color = "green")
        else:
            stimulus = visual.TextStim(win, text=WordAll20[i], color = "blue")
        stimulusLeft = visual.TextStim(win, text= 'Male', pos = (-0.7, 0.8), color = "blue")
        stimulusLeftOr = visual.TextStim(win, text= 'or', pos = (-0.7, 0.7), color = "Black")
        stimulusLeft2 = visual.TextStim(win, text= 'Arts', pos = (-0.7, 0.6), color = "green")
        stimulusRight = visual.TextStim(win, text= 'Female', pos = (0.7, 0.8), color = "blue")
        stimulusRightOr = visual.TextStim(win, text= 'or', pos = (0.7, 0.7), color = "Black")
        stimulusRight2 = visual.TextStim(win, text= 'Science', pos = (0.7, 0.6), color = "green")
        stimulus.draw()
        stimulusLeft.draw()
        stimulusLeftOr.draw()
        stimulusLeft2.draw()
        stimulusRight.draw()
        stimulusRightOr.draw()
        stimulusRight2.draw()
        stimulusX = visual.TextStim(win, text='X', pos = (0.0, -0.6), color = "red")
        stimulusX.draw()
        win.flip()
        correct = 0
        keyX = event.waitKeys(keyList = ['escape', 'e'])
    # append all recorded data to the pandas DATA 
    iat_data = iat_data.append({
        'ID': ID[0],                #make these match the info box 
        'age': ID[1],
        'gender': ID[2],
        'education': ID[3],
        'semester': ID[4],
        'word': WordAll20[i],
        'response': key[0],
        'correct': correct, 
        'rt': reaction_time,
        'block': '3', #change for very loop/block
        'trial': trial[i]
        }, ignore_index=True)
    previous = WordAll20[i]




block4 = '''Sort the same four categories again. Remember to go as fast as you can while making as few mistakes as possible.

Use the E and I keys to categorize items into the four groups left and right, and correct errors by hitting the other key.

Press "space" to continue'''


# define function that shows text
def msg4(txt):
    instructions = visual.TextStim(win, text=txt, color = 'Black', height = 0.05) # create an instruction text
    stimulusLeft = visual.TextStim(win, text= 'Male', pos = (-0.7, 0.8), color = "blue")
    stimulusLeftOr = visual.TextStim(win, text= 'or', pos = (-0.7, 0.7), color = "Black")
    stimulusLeft2 = visual.TextStim(win, text= 'Arts', pos = (-0.7, 0.6), color = "green")
    stimulusRight = visual.TextStim(win, text= 'Female', pos = (0.7, 0.8), color = "blue")
    stimulusRightOr = visual.TextStim(win, text= 'or', pos = (0.7, 0.7), color = "Black")
    stimulusRight2 = visual.TextStim(win, text= 'Science', pos = (0.7, 0.6), color = "green")
    instructions.draw()
    stimulusLeft.draw()
    stimulusLeftOr.draw()
    stimulusLeft2.draw()
    stimulusRight.draw()
    stimulusRightOr.draw() 
    stimulusRight2.draw() # draw the text stimulus in a "hidden screen" so that it is ready to be presented 
    win.flip() # flip the screen to reveal the stimulusi
    event.waitKeys(keyList = ['space'])


msg4(block4)


###BLOCK 4
# loop through trials
for i in range(len(WordAll40)):
    # prepare stimulus
    while WordAll40[i] == previous: 
        WordAll40[i] = random.sample(WordAll40,1)[0]
        if WordAll40[i] != previous: 
            break
    if WordAll40[i] in WordScience:
        stimulus = visual.TextStim(win, text=WordAll40[i], color = "green")
    else:
        stimulus = visual.TextStim(win, text=WordAll40[i], color = "blue")
    stimulusLeft = visual.TextStim(win, text= 'Male', pos = (-0.7, 0.8), color = "blue")
    stimulusLeftOr = visual.TextStim(win, text= 'or', pos = (-0.7, 0.7), color = "Black")
    stimulusLeft2 = visual.TextStim(win, text= 'Arts', pos = (-0.7, 0.6), color = "green")
    stimulusRight = visual.TextStim(win, text= 'Female', pos = (0.7, 0.8), color = "blue")
    stimulusRightOr = visual.TextStim(win, text= 'or', pos = (0.7, 0.7), color = "Black")
    stimulusRight2 = visual.TextStim(win, text= 'Science', pos = (0.7, 0.6), color = "green")
    # draw stimulus
    stimulus.draw()
    stimulusLeft.draw()
    stimulusLeftOr.draw()
    stimulusLeft2.draw()
    stimulusRight.draw()
    stimulusRightOr.draw()
    stimulusRight2.draw()
    win.flip()
    # reset stop watch
    stopwatch.reset()                              #reset the clock to 0:0:0 
    # record key press
    key = event.waitKeys(keyList = ['escape', 'e', 'i'])                  # wait for any key press
    # get reaction time at key press
    reaction_time = stopwatch.getTime()            # asks the stopwatch for the time since reset and save to the variable reation_time
    
    # check if response is correct and if it is congruent 
    if key[0] == 'e' and (WordAll40[i] in WordArt or WordAll40[i] in WordMale):
        correct = 1
    elif key[0] == 'i' and (WordAll40[i] in WordSci or WordAll40[i] in WordFemale):
        correct = 1
    elif key[0] == 'escape':
        core.quit()
        win.close()
    elif key[0] == 'e' and (WordAll40[i] in WordSci or WordAll40[i] in WordFemale):
        if WordAll40[i] in WordScience:
            stimulus = visual.TextStim(win, text=WordAll40[i], color = "green")
        else:
            stimulus = visual.TextStim(win, text=WordAll40[i], color = "blue")
        stimulusLeft = visual.TextStim(win, text= 'Male', pos = (-0.7, 0.8), color = "blue")
        stimulusLeftOr = visual.TextStim(win, text= 'or', pos = (-0.7, 0.7), color = "Black")
        stimulusLeft2 = visual.TextStim(win, text= 'Arts', pos = (-0.7, 0.6), color = "green")
        stimulusRight = visual.TextStim(win, text= 'Female', pos = (0.7, 0.8), color = "blue")
        stimulusRightOr = visual.TextStim(win, text= 'or', pos = (0.7, 0.7), color = "Black")
        stimulusRight2 = visual.TextStim(win, text= 'Science', pos = (0.7, 0.6), color = "green")
        stimulus.draw()
        stimulusLeft.draw()
        stimulusLeftOr.draw()
        stimulusLeft2.draw()
        stimulusRight.draw()
        stimulusRightOr.draw()
        stimulusRight2.draw()
        stimulusX = visual.TextStim(win, text='X', pos = (0.0, -0.6), color = "red")
        stimulusX.draw()
        win.flip()
        correct = 0
        keyX = event.waitKeys(keyList = ['escape', 'i'])
    else: 
        if WordAll40[i] in WordScience:
            stimulus = visual.TextStim(win, text=WordAll40[i], color = "green")
        else:
            stimulus = visual.TextStim(win, text=WordAll40[i], color = "blue")
        stimulusLeft = visual.TextStim(win, text= 'Male', pos = (-0.7, 0.8), color = "blue")
        stimulusLeftOr = visual.TextStim(win, text= 'or', pos = (-0.7, 0.7), color = "Black")
        stimulusLeft2 = visual.TextStim(win, text= 'Arts', pos = (-0.7, 0.6), color = "green")
        stimulusRight = visual.TextStim(win, text= 'Female', pos = (0.7, 0.8), color = "blue")
        stimulusRightOr = visual.TextStim(win, text= 'or', pos = (0.7, 0.7), color = "Black")
        stimulusRight2 = visual.TextStim(win, text= 'Science', pos = (0.7, 0.6), color = "green")
        stimulus.draw()
        stimulusLeft.draw()
        stimulusLeftOr.draw()
        stimulusLeft2.draw()
        stimulusRight.draw()
        stimulusRightOr.draw()
        stimulusRight2.draw()
        stimulusX = visual.TextStim(win, text='X', pos = (0.0, -0.6), color = "red")
        stimulusX.draw()
        win.flip()
        correct = 0
        keyX = event.waitKeys(keyList = ['escape', 'e'])
    # append all recorded data to the pandas DATA 
    iat_data = iat_data.append({
        'ID': ID[0],                #make these match the info box 
        'age': ID[1],
        'gender': ID[2],
        'education': ID[3],
        'semester': ID[4],
        'word': WordAll40[i],
        'response': key[0],
        'correct': correct, 
        'rt': reaction_time,
        'block': '4', #change for very loop/block
        'trial': trial[i]
        }, ignore_index=True)
    previous = WordAll40[i]



block5 = '''Now there are only two cateogries, and they have switched positions. 

Use the E and I keys to catgorize items left and right, and correct errors by hitting the other key.

Press "space" to continue'''


# define function that shows text
def msg5(txt):
    instructions = visual.TextStim(win, text=txt, color = 'Black', height = 0.05) # create an instruction text
    stimulusLeft = visual.TextStim(win, text= 'Science', pos = (-0.7, 0.8), color = "green")
    stimulusRight = visual.TextStim(win, text= 'Arts', pos = (0.7, 0.8), color = "green")
    stimulusLeft.draw()
    stimulusRight.draw()
    instructions.draw() # draw the text stimulus in a "hidden screen" so that it is ready to be presented 
    win.flip() # flip the screen to reveal the stimulusi
    event.waitKeys(keyList = ['space'])


msg5(block5)



###BLOCK 5 
# loop through trials
for i in range(len(WordScience28)):
    # prepare stimulus
    while WordScience28[i] == previous: 
        WordScience28[i] = random.sample(WordScience28,1)[0]
        if WordScience28[i] != previous: 
            break
    stimulus = visual.TextStim(win, text=WordScience28[i], color = "green")
    stimulusLeft = visual.TextStim(win, text= 'Science', pos = (-0.7, 0.8), color = "green")
    stimulusRight = visual.TextStim(win, text= 'Arts', pos = (0.7, 0.8), color = "green")
    # draw stimulus
    stimulus.draw()
    stimulusLeft.draw()
    stimulusRight.draw()
    win.flip()
    # reset stop watch
    stopwatch.reset()                              #reset the clock to 0:0:0 
    # record key press
    key = event.waitKeys(keyList = ['escape', 'e', 'i'])                  # wait for any key press
    # get reaction time at key press
    reaction_time = stopwatch.getTime()            # asks the stopwatch for the time since reset and save to the variable reation_time
    
    # check if response is correct and if it is congruent 
    if key[0] == 'e' and WordScience28[i] in WordSci:
        correct = 1
    elif key[0] == 'i' and WordScience28[i] in WordArt:
        correct = 1
    elif key[0] == 'escape':
        core.quit()
        win.close()
    elif key[0] == 'e' and WordScience28[i] in WordArt:
        stimulus = visual.TextStim(win, text=WordScience28[i], color = "green")
        stimulus.draw()
        stimulusLeft = visual.TextStim(win, text= 'Science', pos = (-0.7, 0.8), color = "green")
        stimulusRight = visual.TextStim(win, text= 'Arts', pos = (0.7, 0.8), color = "green")
        stimulusLeft.draw()
        stimulusRight.draw()
        stimulusX = visual.TextStim(win, text='X', pos = (0.0, -0.6), color = "red")
        stimulusX.draw()
        win.flip()
        correct = 0
        keyX = event.waitKeys(keyList = ['escape', 'i'])
    else: 
        stimulus = visual.TextStim(win, text=WordScience28[i], color = "green")
        stimulus.draw()
        stimulusLeft = visual.TextStim(win, text= 'Science', pos = (-0.7, 0.8), color = "green")
        stimulusRight = visual.TextStim(win, text= 'Arts', pos = (0.7, 0.8), color = "green")
        stimulusLeft.draw()
        stimulusRight.draw()
        stimulusX = visual.TextStim(win, text='X', pos = (0.0, -0.6), color = "red")
        stimulusX.draw()
        win.flip()
        correct = 0
        keyX = event.waitKeys(keyList = ['escape', 'e'])
  # append all recorded data to the pandas DATA 
    iat_data = iat_data.append({
        'ID': ID[0],                #make these match the info box 
        'age': ID[1],
        'gender': ID[2],
        'education': ID[3],
        'semester': ID[4],
        'word': WordScience28[i],
        'response': key[0],
        'correct': correct, 
        'rt': reaction_time,
        'block': '5', #change for very loop/block
        'trial': trial[i]
        }, ignore_index=True)
    previous = WordScience28[i]




block6 = '''See above, the four categories now appear together in a new configuration. Remember, each item belongs to only one group.

Use the E and I keys to categorize items into the four groups left and right, and correct errors by hitting the other key.

Press "space" to continue'''


# define function that shows text
def msg6(txt):
    instructions = visual.TextStim(win, text=txt, color = 'Black', height = 0.05) # create an instruction text
    stimulusLeft = visual.TextStim(win, text= 'Male', pos = (-0.7, 0.8), color = "blue") 
    stimulusLeftOr = visual.TextStim(win, text= 'or', pos = (-0.7, 0.7), color = "Black")
    stimulusLeft2 = visual.TextStim(win, text= 'Science', pos = (-0.7, 0.6), color = "green")
    stimulusRight = visual.TextStim(win, text= 'Female', pos = (0.7, 0.8), color = "blue")
    stimulusRightOr = visual.TextStim(win, text= 'or', pos = (0.7, 0.7), color = "Black")
    stimulusRight2 = visual.TextStim(win, text= 'Arts', pos = (0.7, 0.6), color = "green")
    instructions.draw()
    stimulusLeft.draw()
    stimulusLeftOr.draw()
    stimulusLeft2.draw()
    stimulusRight.draw()
    stimulusRightOr.draw()
    stimulusRight2.draw()# draw the text stimulus in a "hidden screen" so that it is ready to be presented 
    win.flip() # flip the screen to reveal the stimulusi
    event.waitKeys(keyList = ['space'])


msg6(block6)


###BLOCK 6
# loop through trials
for i in range(len(WordAll20)):
    # prepare stimulus
    while WordAll20[i] == previous: 
        WordAll20[i] = random.sample(WordAll20,1)[0]
        if WordAll20[i] != previous: 
            break
    if WordAll20[i] in WordScience:
        stimulus = visual.TextStim(win, text=WordAll20[i], color = "green")
    else:
        stimulus = visual.TextStim(win, text=WordAll20[i], color = "blue")
    stimulusLeft = visual.TextStim(win, text= 'Male', pos = (-0.7, 0.8), color = "blue") 
    stimulusLeftOr = visual.TextStim(win, text= 'or', pos = (-0.7, 0.7), color = "Black")
    stimulusLeft2 = visual.TextStim(win, text= 'Science', pos = (-0.7, 0.6), color = "green")
    stimulusRight = visual.TextStim(win, text= 'Female', pos = (0.7, 0.8), color = "blue")
    stimulusRightOr = visual.TextStim(win, text= 'or', pos = (0.7, 0.7), color = "Black")
    stimulusRight2 = visual.TextStim(win, text= 'Arts', pos = (0.7, 0.6), color = "green")
    # draw stimulus
    stimulus.draw()
    stimulusLeft.draw()
    stimulusLeftOr.draw()
    stimulusLeft2.draw()
    stimulusRight.draw()
    stimulusRightOr.draw()
    stimulusRight2.draw()
    win.flip()
    # reset stop watch
    stopwatch.reset()                              #reset the clock to 0:0:0 
    # record key press
    key = event.waitKeys(keyList = ['escape', 'e', 'i'])                  # wait for any key press
    # get reaction time at key press
    reaction_time = stopwatch.getTime()            # asks the stopwatch for the time since reset and save to the variable reation_time
    
    # check if response is correct and if it is congruent 
    if key[0] == 'e' and (WordAll20[i] in WordSci or WordAll20[i] in WordMale): 
        correct = 1
    elif key[0] == 'i' and (WordAll20[i] in WordArt or WordAll20[i] in WordFemale):
        correct = 1
    elif key[0] == 'escape':
        core.quit()
        win.close()
    elif key[0] == 'e' and (WordAll20[i] in WordArt or WordAll20[i] in WordFemale):
        if WordAll20[i] in WordScience:
            stimulus = visual.TextStim(win, text=WordAll20[i], color = "green")
        else:
            stimulus = visual.TextStim(win, text=WordAll20[i], color = "blue")
        stimulusLeft = visual.TextStim(win, text= 'Male', pos = (-0.7, 0.8), color = "blue")
        stimulusLeftOr = visual.TextStim(win, text= 'or', pos = (-0.7, 0.7), color = "Black")
        stimulusLeft2 = visual.TextStim(win, text= 'Science', pos = (-0.7, 0.6), color = "green")
        stimulusRight = visual.TextStim(win, text= 'Female', pos = (0.7, 0.8), color = "blue")
        stimulusRightOr = visual.TextStim(win, text= 'or', pos = (0.7, 0.7), color = "Black")
        stimulusRight2 = visual.TextStim(win, text= 'Arts', pos = (0.7, 0.6), color = "green")
        stimulus.draw()
        stimulusLeft.draw()
        stimulusLeftOr.draw()
        stimulusLeft2.draw()
        stimulusRight.draw()
        stimulusRightOr.draw()
        stimulusRight2.draw()
        stimulusX = visual.TextStim(win, text='X', pos = (0.0, -0.6), color = "red")
        stimulusX.draw()
        win.flip()
        correct = 0
        keyX = event.waitKeys(keyList = ['escape', 'i'])
    else: 
        if WordAll20[i] in WordScience:
            stimulus = visual.TextStim(win, text=WordAll20[i], color = "green")
        else:
            stimulus = visual.TextStim(win, text=WordAll20[i], color = "blue")
        stimulusLeft = visual.TextStim(win, text= 'Male', pos = (-0.7, 0.8), color = "blue")
        stimulusLeftOr = visual.TextStim(win, text= 'or', pos = (-0.7, 0.7), color = "Black")
        stimulusLeft2 = visual.TextStim(win, text= 'Science', pos = (-0.7, 0.6), color = "green")
        stimulusRight = visual.TextStim(win, text= 'Female', pos = (0.7, 0.8), color = "blue")
        stimulusRightOr = visual.TextStim(win, text= 'or', pos = (0.7, 0.7), color = "Black")
        stimulusRight2 = visual.TextStim(win, text= 'Arts', pos = (0.7, 0.6), color = "green")
        stimulus.draw()
        stimulusLeft.draw()
        stimulusLeftOr.draw()
        stimulusLeft2.draw()
        stimulusRight.draw()
        stimulusRightOr.draw()
        stimulusRight2.draw()
        stimulusX = visual.TextStim(win, text='X', pos = (0.0, -0.6), color = "red")
        stimulusX.draw()
        win.flip()
        correct = 0
        keyX = event.waitKeys(keyList = ['escape', 'e'])
    # append all recorded data to the pandas DATA 
    iat_data = iat_data.append({
        'ID': ID[0],                #make these match the info box 
        'age': ID[1],
        'gender': ID[2],
        'education': ID[3],
        'semester': ID[4],
        'word': WordAll20[i],
        'response': key[0],
        'correct': correct, 
        'rt': reaction_time,
        'block': '6', #change for very loop/block
        'trial': trial[i]
        }, ignore_index=True)
    previous = WordAll20[i]



block7 = '''Sort the same four categories again. 

Use the E and I keys to categorize items into the four groups left and right, and correct errors by hitting the other key.

Press "space" to continue'''


# define function that shows text
def msg7(txt):
    instructions = visual.TextStim(win, text=txt, color = 'Black', height = 0.05) # create an instruction text
    stimulusLeft = visual.TextStim(win, text= 'Male', pos = (-0.7, 0.8), color = "blue") 
    stimulusLeftOr = visual.TextStim(win, text= 'or', pos = (-0.7, 0.7), color = "Black")
    stimulusLeft2 = visual.TextStim(win, text= 'Science', pos = (-0.7, 0.6), color = "green")
    stimulusRight = visual.TextStim(win, text= 'Female', pos = (0.7, 0.8), color = "blue")
    stimulusRightOr = visual.TextStim(win, text= 'or', pos = (0.7, 0.7), color = "Black")
    stimulusRight2 = visual.TextStim(win, text= 'Arts', pos = (0.7, 0.6), color = "green")
    instructions.draw()
    stimulusLeft.draw()
    stimulusLeftOr.draw()
    stimulusLeft2.draw()
    stimulusRight.draw()
    stimulusRightOr.draw()
    stimulusRight2.draw()# draw the text stimulus in a "hidden screen" so that it is ready to be presented 
    win.flip() # flip the screen to reveal the stimulusi
    event.waitKeys(keyList = ['space'])


msg7(block7)


###BLOCK 7
# loop through trials
for i in range(len(WordAll40)):
    # prepare stimulus
    while WordAll40[i] == previous: 
        WordAll40[i] = random.sample(WordAll40,1)[0]
        if WordAll40[i] != previous: 
            break
    if WordAll40[i] in WordScience:
        stimulus = visual.TextStim(win, text=WordAll40[i], color = "green")
    else:
        stimulus = visual.TextStim(win, text=WordAll40[i], color = "blue")
    stimulusLeft = visual.TextStim(win, text= 'Male', pos = (-0.7, 0.8), color = "blue")
    stimulusLeftOr = visual.TextStim(win, text= 'or', pos = (-0.7, 0.7), color = "Black")
    stimulusLeft2 = visual.TextStim(win, text= 'Science', pos = (-0.7, 0.6), color = "green")
    stimulusRight = visual.TextStim(win, text= 'Female', pos = (0.7, 0.8), color = "blue")
    stimulusRightOr = visual.TextStim(win, text= 'or', pos = (0.7, 0.7), color = "Black")
    stimulusRight2 = visual.TextStim(win, text= 'Arts', pos = (0.7, 0.6), color = "green")
    # draw stimulus
    stimulus.draw()
    stimulusLeft.draw()
    stimulusLeftOr.draw()
    stimulusLeft2.draw()
    stimulusRight.draw()
    stimulusRightOr.draw()
    stimulusRight2.draw()
    win.flip()
    # reset stop watch
    stopwatch.reset()                              #reset the clock to 0:0:0 
    # record key press
    key = event.waitKeys(keyList = ['escape', 'e', 'i'])                  # wait for any key press
    # get reaction time at key press
    reaction_time = stopwatch.getTime()            # asks the stopwatch for the time since reset and save to the variable reation_time
    
    # check if response is correct and if it is congruent 
    if key[0] == 'e' and (WordAll40[i] in WordSci or WordAll40[i] in WordMale):
        correct = 1
    elif key[0] == 'i' and (WordAll40[i] in WordArt or WordAll40[i] in WordFemale):
        correct = 1
    elif key[0] == 'escape':
        core.quit()
        win.close()
    elif key[0] == 'e' and (WordAll40[i] in WordArt or WordAll40[i] in WordFemale):
        if WordAll40[i] in WordScience:
            stimulus = visual.TextStim(win, text=WordAll40[i], color = "green")
        else:
            stimulus = visual.TextStim(win, text=WordAll40[i], color = "blue")
        stimulusLeft = visual.TextStim(win, text= 'Male', pos = (-0.7, 0.8), color = "blue")
        stimulusLeftOr = visual.TextStim(win, text= 'or', pos = (-0.7, 0.7), color = "Black")
        stimulusLeft2 = visual.TextStim(win, text= 'Science', pos = (-0.7, 0.6), color = "green")
        stimulusRight = visual.TextStim(win, text= 'Female', pos = (0.7, 0.8), color = "blue")
        stimulusRightOr = visual.TextStim(win, text= 'or', pos = (0.7, 0.7), color = "Black")
        stimulusRight2 = visual.TextStim(win, text= 'Arts', pos = (0.7, 0.6), color = "green")
        stimulus.draw()
        stimulusLeft.draw()
        stimulusLeftOr.draw()
        stimulusLeft2.draw()
        stimulusRight.draw()
        stimulusRightOr.draw()
        stimulusRight2.draw()
        stimulusX = visual.TextStim(win, text='X', pos = (0.0, -0.6), color = "red")
        stimulusX.draw()
        win.flip()
        correct = 0
        keyX = event.waitKeys(keyList = ['escape', 'i'])
    else: 
        if WordAll40[i] in WordScience:
            stimulus = visual.TextStim(win, text=WordAll40[i], color = "green")
        else:
            stimulus = visual.TextStim(win, text=WordAll40[i], color = "blue")
        stimulusLeft = visual.TextStim(win, text= 'Male', pos = (-0.7, 0.8), color = "blue")
        stimulusLeftOr = visual.TextStim(win, text= 'or', pos = (-0.7, 0.7), color = "Black")
        stimulusLeft2 = visual.TextStim(win, text= 'Science', pos = (-0.7, 0.6), color = "green")
        stimulusRight = visual.TextStim(win, text= 'Female', pos = (0.7, 0.8), color = "blue")
        stimulusRightOr = visual.TextStim(win, text= 'or', pos = (0.7, 0.7), color = "Black")
        stimulusRight2 = visual.TextStim(win, text= 'Arts', pos = (0.7, 0.6), color = "green")
        stimulus.draw()
        stimulusLeft.draw()
        stimulusLeftOr.draw()
        stimulusLeft2.draw()
        stimulusRight.draw()
        stimulusRightOr.draw()
        stimulusRight2.draw()
        stimulusX = visual.TextStim(win, text='X', pos = (0.0, -0.6), color = "red")
        stimulusX.draw()
        win.flip()
        correct = 0
        keyX = event.waitKeys(keyList = ['escape', 'e'])
    # append all recorded data to the pandas DATA 
    iat_data = iat_data.append({
        'ID': ID[0],                #make these match the info box 
        'age': ID[1],
        'gender': ID[2],
        'education': ID[3],
        'semester': ID[4],
        'word': WordAll40[i],
        'response': key[0],
        'correct': correct, 
        'rt': reaction_time,
        'block': '7', #change for very loop/block
        'trial': trial[i]
        }, ignore_index=True)
    previous = WordAll40[i]



#print(iat_data)

logfile_name = 'logfile_' + ID[0] + '.csv'
iat_data.to_csv(logfile_name)


outro = '''The experiment is done. Thank you for your participation!

Now, please fill out the questionnaire online about explicit stereotypes'''


msg(outro)
core.quit()

