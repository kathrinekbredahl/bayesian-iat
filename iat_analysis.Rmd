
ANALYSIS OF THE DATA

```{r}
library(pacman)
library(ggplot2)
library(rethinking)
library(brms)
library(plyr)
library(tidyverse)
library(devtools)
library(DataCombine)

setwd("C:/Users/kathr/OneDrive/COGNITIVE SCIENCE/4. semester/Social and cultural dynamics in cognition/Exam")
```

LOADING DATA 
```{r}
iat_data <- read.csv("C:\\Users\\kathr\\OneDrive\\COGNITIVE SCIENCE\\4. semester\\Social and cultural dynamics in cognition\\Exam\\iat_data.csv")
#file.choose()

iat_data$X <- NULL

#head(iat_data)

```

HYPOTHESIS 1 AND 2

PRIOR GENERATING 
```{r}

#range on y 
log(0.3) 
log(10) 
#exp() to find the "true" rt 



#to figure out which prior I need to make 
get_prior(rtlog ~ condition * education + (1 + condition|ID) + (1|gender), data = iat_data)


#creating a skeptical prior
prior = c(prior(normal(0.5, 1.5), class = "Intercept"), 
          prior(normal(0, 0.5), class = "b"), 
          prior(normal(0, 0.5), class = "sigma"),
          prior(normal(0, 0.1), class = "sd"))

```


MODEL COMPARISON 
```{r}

#the model I believe will explain the data 
model_con_edu_prior <- brm(rtlog ~ condition * education + (1 + condition|ID),
                    data = iat_data,
                    family = gaussian(),
                    prior = prior,
                    sample_prior = "only",
                    cores = 2,
                    chains = 2)

pp_check(model_con_edu_prior)

model_con_edu <- brm(rtlog ~ condition * education + (1 + condition|ID),
                    data = iat_data,
                    family = gaussian(),
                    prior = prior,
                    sample_prior = TRUE,
                    iter = 5000,
                    warmup = 2000,
                    cores = 2,
                    chains = 2)

summary(model_con_edu)
pp_check(model_con_edu)
plot(model_con_edu)


#same main effects and random effects, but no interaction 
model_con_plus_edu_prior <- brm(rtlog ~ condition + education + (1 + condition|ID),
                    data = iat_data,
                    family = gaussian(),
                    prior = prior,
                    sample_prior = "only",
                    cores = 2,
                    chains = 2)

pp_check(model_con_plus_edu_prior)

model_con_plus_edu <- brm(rtlog ~ condition + education + (1 + condition|ID),
                    data = iat_data,
                    family = gaussian(),
                    prior = prior,
                    sample_prior = TRUE,
                    iter = 5000,
                    warmup = 2000,
                    cores = 2,
                    chains = 2)

summary(model_con_plus_edu)
pp_check(model_con_plus_edu)
plot(model_con_plus_edu)


#same random effects but only one of the main effects 
model_con_prior <- brm(rtlog ~ condition + (1 + condition|ID),
                    data = iat_data,
                    family = gaussian(),
                    prior = prior,
                    sample_prior = "only",
                    cores = 2,
                    chains = 2)

pp_check(model_con_prior)

model_con <- brm(rtlog ~ condition + (1 + condition|ID),
                    data = iat_data,
                    family = gaussian(),
                    prior = prior,
                    sample_prior = TRUE,
                    iter = 5000,
                    warmup = 2000,
                    cores = 2,
                    chains = 2)

summary(model_con)
pp_check(model_con)
plot(model_con)

#same random effects, but only the other main effect 
model_edu_prior <- brm(rtlog ~ education + (1 + condition|ID),
                    data = iat_data,
                    family = gaussian(),
                    prior = prior,
                    sample_prior = "only",
                    cores = 2,
                    chains = 2)

pp_check(model_edu_prior)

model_edu <- brm(rtlog ~ education + (1 + condition|ID),
                    data = iat_data,
                    family = gaussian(),
                    prior = prior,
                    sample_prior = TRUE,
                    iter = 5000,
                    warmup = 2000,
                    cores = 2,
                    chains = 2)

summary(model_edu)
pp_check(model_edu)
plot(model_edu)


#comparing the models to investigate which is closest to the true model 
waic(model_con_edu, model_con_plus_edu, model_con, model_edu)

model_weights(model_con_edu, model_con_plus_edu, model_con, model_edu, weights = "waic")
#model_con_edu model_con_plus_edu          model_con          model_edu 
#        0.33823885         0.26999584         0.30919532         0.08256999 
 

```

HYPOTHESES TESTING 
```{r}

#investigating hypothesis 1:
#reaction times for congruent blocks (science-male) will be smaller than reaction times for incongruent blocks (science-female), indicating implicit bias

hypothesis(model_con_edu, "condition < 0") #results: people are biased! 


#investigating hypothesis 2: 
#reaction times for both congruent and incongruent blocks will be the same, indicating no implicit bias


summary(model_con_edu)

hypothesis(model_con_edu, "condition:educationCognitive Science = 0") #= 0 no effect 
hypothesis(model_con_edu, "condition:educationPhilosophy = 0")

#educationCognitiveScience and educationPhilosphy = these are the effect or education, i.e. cogsci being faster than bio and philo being faster than bio too - but that's what I get out of this!
#education effect is about rt! not condition 


#interaction = education has no effect on condition i.e. 
#the importance of education on the effect of condition, on the bias 
#ci crossing 0??? 
#in interaction error are larger than effect 

hypothesis(model_con_edu, "educationCognitiveScience = 0") #cogsci and bio being equally fast in overall rt 
hypothesis(model_con_edu, "educationPhilosophy = 0") #philo and bio being equally fast in overall rt 
#effect of education = education effect is about rt, how the educations rts are in realtion to each other 


```



HYPOTHESIS 3
```{r}

#loading explicit data
explicit_science_score <- read.csv("C:\\Users\\kathr\\OneDrive\\COGNITIVE SCIENCE\\4. semester\\Social and cultural dynamics in cognition\\Exam\\Explicit stereotypes\\science_score.csv")

explicit_science_score$X <- NULL


#all random effects 
allranef <- data.frame(ranef(model_con_edu)$ID)



#extracting implicit congruent data
randomeff <- data.frame(ranef(model_con_edu)$ID)[,5]
randomeff <- data.frame(randomeff)

randomeff$ID <- c("AJN",                  "ALA",                  "ALB" ,                 "Amalie"     ,          "AMBB",                
"Anders"            ,   "Anders H" ,            "Anna Konnerup",        "Asbjørn" ,            "au56"    ,            
"AU578816"          ,   "BH"        ,           "bio"           ,       "Bondemanden",          "cb"       ,           
"Celine"             ,  "CST"        ,          "cykelmyggen"    ,      "Daniel"      ,         "DT"        ,          
"DVN"                 , "Emil"        ,         "EMJ"             ,     "Færing"      ,        "GrandMan"   ,         
"Halløjsovs"          ,"Helene"       ,        "History Man"      ,    "Ingharstef"    ,       "J.T.D"       ,        
"Jacob"       ,         "JB"            ,       "Jennie"            ,   "JLE"            ,      "JNH"          ,       
"Johan"        ,        "Jon Snow"       ,      "Katrine Ottosen"    ,  "KEP"             ,     "LAJ"           ,      
"Lime"          ,       "Line"            ,     "LJA"                 , "Malte"            ,    "Martin"        ,     
"MEA"            ,      "MLHH"             ,    "MSWN"        ,         "not_emil_rønn"    ,   "sartrefan1234"   ,    
"SigneH"          ,     "snapekillsdumbledore", "Sokrates"     ,        "ssp"                ,  "Sveske"           ,   
"TDK"              ,    "tkv"                 , "uy56"          ,       "VMP"                 , "yqy")

colnames(randomeff) <- c("estimate", "ID")

#dataframe containing random effect estimates and explicit scores 
hyp3data <- join(explicit_science_score, randomeff, type = "left")


#model to investigate the relationship between the implicit rt and the explicit scores 
cor(hyp3data$estimate, hyp3data$science_gender_score, method = "spearman")
# -0.2693028 #somehow the slope of the line in marginal plot - but here is not really any correlation 
#spearman bc it is a nonparametric test and does not carry any assumptions about the distributions of the data

```

PLOTTING DATA 
```{r}

iat_data$condition <- as.factor(iat_data$condition)

#HYP 1 AND 2 
#investigating reaction times according to conditions, how fast were people 
ggplot(iat_data, aes(x = rt, fill = condition)) + 
  scale_fill_manual(values = c("green", "orange")) +
  geom_density() + 
  facet_wrap(~ education) + 
  labs(title = "Reaction times across educations")


hyp3data$gender <- as.factor(hyp3data$gender)


#correlation test: the data in points and a line through
ggplot(hyp3data, aes(x = science_gender_score, y = estimate)) +
  scale_color_manual(values=c("violet", "blue")) +
  geom_point() +
  geom_point(aes(color = factor(hyp3data$gender))) +
  geom_smooth(method = "lm", color = "black") +
  labs(title = "Implicit and explicit relationship")


#this plot shows that people get better in 4th and 7th block according to rt, especially in incongruent -> practice helps
#future models could account for this?
ggplot(iat_data, aes(x=block, y = rt, color = condition))+
geom_point(alpha = 0.1)+
  geom_smooth(method = "lm")


ggplot(data = hyp3data, (aes(x = semester, fill = education))) +  
  scale_fill_brewer(palette="Set1") +
  geom_bar() +
  labs(title = "Participants: semester overview") + 
  facet_wrap(~gender)


#explicit associations 
ggplot(hyp3data, aes(science_gender_score, fill = gender)) +
  scale_fill_manual(values = c("#CC0033","#00CC99")) +
  geom_bar() + 
  facet_wrap(~education) +
  labs(title = "Association between science and gender")


```

